<?php


namespace App\Http\Controllers;

use App\Models\User;
use Firebase\JWT\JWT;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;

class UserController extends Controller
{
    public function register(Request $request)
    {
        $this->validate($request, [
           'name' => 'required|unique:users',
           'password' => 'required'
        ]);

        try {
            $user = new User();
            $user->name = $request->get('name');
            $user->password = $request->get('password');
            $user->save();
        } catch (QueryException $queryException) {
            return response()->json(['error' => $queryException->getMessage()], 400);
        }

        return response()->json(['user' => $user, 'message' => 'CREATED'], 201);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'password' => 'required'
        ]);

        $user = User::where([
            'name' => $request->get('name'),
            'password' => $request->get('password')
        ])->first();

        if (!$user) {
            return response()->json(['error' => 'Could not find user'], 400);
        }

        $issuedat_claim = time(); // issued at
        $expire_claim = $issuedat_claim + 600; // expire time in seconds

        // Not mandatory, but recommended registered claim names in payload
        // https://datatracker.ietf.org/doc/html/rfc7519#section-4.1

        $token =  [
            'iss' => env('SERVER_NAME'),
            'iat' => $issuedat_claim,
            'exp' => $expire_claim,
            'data' => [
                'name' => 'tesztname'
            ]
        ];

        $jwt = JWT::encode($token, env('JWT_SECRET'), 'HS256');

        return response()->json($jwt, 200);
    }

    public function getAllUsers(Request $request)
    {
        try {
            $credentials = JWT::decode($request->bearerToken(), env('JWT_SECRET'), ['HS256']);
        } catch (\Exception $exception) {
            return response()->json(['error' => 'Invalid token: ' . $exception->getMessage()], 400);
        }

        return response()->json(User::all(), 200);
    }
}