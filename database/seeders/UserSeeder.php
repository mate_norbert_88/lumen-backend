<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'teszt',
                'password' => '123456'
            ],
            [
                'name' => 'jóska',
                'password' => '123456'
            ],
            [
                'name' => 'pista',
                'password' => '123456'
            ],
        ]);
    }
}
