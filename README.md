# Config
Az .env.example-ben találsz két környezeti változót, ez kell majd a .env-be:
<ul>
    <li>JWT_SECRET</li>
    <li>SERVER_NAME</li>
</ul>

Az adatbázis beállítás szintén benne van a .env.example-ben, Postgreshez csatlakozik, 
ami pedig fut egy docker containerben.

# CORS
A CORS miatt tettem bele egy Middleware-t, hogy működjön bármelelyik domain hívásból.

#JWT
A firebase/php-jwt csomagot használtam. A UserControllerben találod most (lehetne ez is middleware),
token generáláshoz a JWT::encode metódust, token validáláshoz pedig a JWT::decode fgv-t kell használni. \
A tokent a HTTP kérés authorizációs headerből veszi (bearer token).